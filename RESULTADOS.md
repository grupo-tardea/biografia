# RESULTADOS
**Al analizar la función de cada uno de las opciones que genera los diferentes tipos de permisos para otras personas para revisar, editar o simplemente ver un repositorio.**

## Como Guess: 
*  No se puede crear ramas ni hacer ningun tipo de modificación.
*  No se puede crear archivos de ninguna clase.

## Como Repoerter: 
* Puedo crear ramas de forma local, pero no puedo hacer push a estas creaciones.
* Se puede crear archivos de forma local.


## Como Developer: 
* Se puede generar ramas.
* Se puede eliminar ramas.
* Se puede crear archivos y agregarlos al repositorio.
* No se puede hacer merge a la rama master.
* Si se puede hacer merge a ramas secundarias.


## Como Manteiner: 
* Se puede generar ramas
* Se puede eliminar ramas.
* Se puede crear archivos y agregarlos al repositorio. 
* Se puede hacer merge a ramas secundarias.
* Se puede hacer merge a la rama master.

